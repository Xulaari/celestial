// celestial is a loader coded in rust, that is dependant on a main injector program thatll actually inject. this is barebones as hell.

use std::time;
use std::{time::Duration, path};
use std::thread::sleep;
use std::process::Command;
use std::path::Path;
use sysinfo::{ProcessExt, System, SystemExt};

fn is_program_open(program: &str) -> bool {
    let s = System::new_all();
    s.processes().iter().any(|(_, process)| process.name() == program)
}

fn auto_inject() {
    // itll break when the program (roblox in this case) is open, allowing the program to launch the actual injector.

    loop {
        if is_program_open("RobloxPlayerBeta.exe") && ! is_program_open("RobloxPlayerLauncher.exe") {
            break;
        }
    
        sleep(Duration::from_secs(5));
    }
}

fn main() {
    println!("[warning] entry: checking if any external directory exists");

    let dx9ware_injector = "nil"; // set to path of main injector
    let celex_injector = "nil"; // set to path of main injector

    let main_injector;

    if Path::new(dx9ware_injector).exists() {
        println!("[warning] dx9ware directory found, setting injection target to dx9ware.");

        main_injector = dx9ware_injector;
    } else if Path::new(celex_injector).exists() {
        println!("[warning] celex directory found, setting injection target to celex.");

        main_injector = celex_injector;
    } else {
        println!("\n[warning] exiting, no injector path found.");

        sleep(Duration::from_secs(3));

        return;
    }

    print!("[warning] checking for roblox. ");

    if is_program_open("RobloxPlayerBeta.exe") && ! is_program_open("RobloxPlayerLauncher.exe") {
        println!("roblox open.\n")
    } else {
        println!("going into autoinject.\n");
        auto_inject()
    }

    let _ = Command::new(&main_injector)
        .stdin(std::process::Stdio::null())
        .stdout(std::process::Stdio::null())
        .spawn();
}