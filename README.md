# Celestial

Celestial is a Roblox External Injector coded in Rust, allowing you to auto inject any external you have.

The primary gap-filler feature is speed. Rust is a very fast programming language unlike Python, which most loaders are coded in (due to its simplicity).

## Celestial Status

⚠️ Celestial is in the start of development. Expect it to be very barebones. Feel free to send a pull request.

---

## Installing Celestial

Rust programs are very easy to compile, though, since this is coded in Linux, You will have to compile it yourself. No release is provided, Copy these commands into your terminal:

```sh
git clone https://gitlab.com/Xulaari/celestial
cd celestial
cargo build --release
```